# TIPS

- Windows npm upgrade

    ```
    Set-ExecutionPolicy Unrestricted -Scope CurrentUser -Force
    npm install -g npm-windows-upgrade
    npm-windows-upgrade
    ```
    [出处](https://stackoverflow.com/questions/18412129/how-can-i-update-npm-on-windows)
    
    实际测试不行。。。找不到版本大于6的node


- 有些小技巧挺有意思，比如学习lua程序设计就有：
  - 取模  a/b  可以通过这个实现： a - floor(a/b)b，如果是我就老老实实用循环了
  - 构建一个链表：
    ```
    list=nil
    for line=io.lines() do
        list = {next=list, value=line}
    end
    ```
    上面会构建一个链表，真是精巧的设计，不看书真是想不到
    

- 新买的云主机，安装了openresty，再按照luarocks：
    ```
    wget http://luarocks.org/releases/luarocks-2.4.4.tar.gz
    tar -xzvf luarocks-2.4.4.tar.gz
    cd luarocks-2.4.4/
    ./configure --prefix=/usr/local/openresty/luajit \
        --with-lua=/usr/local/openresty/luajit/ \
        --lua-suffix=jit \
        --with-lua-include=/usr/local/openresty/luajit/include/luajit-2.1
    make
    make install
    
    ```

    通过luarocks安装lapis失败：
    
    ```
    Installing https://luarocks.org/lapis-1.7.0-1.src.rock
    Missing dependencies for lapis 1.7.0-1:
       luaossl (not installed)
       luafilesystem (not installed)
       luasocket (not installed)
       mimetypes (not installed)
       pgmoon (not installed)
    
    lapis 1.7.0-1 depends on luaossl (not installed)
    Installing https://luarocks.org/luaossl-20180530-0.src.rock
    
    Error: Failed installing dependency: https://luarocks.org/luaossl-20180530-0.src.rock - Could not find header file for CRYPTO
      No file openssl/crypto.h in /usr/local/include
      No file openssl/crypto.h in /usr/include
    You may have to install CRYPTO in your system and/or pass CRYPTO_DIR or CRYPTO_INCDIR to the luarocks command.
    Example: luarocks install luaossl CRYPTO_DIR=/usr/local
    
    ```
    安装了一堆东西都还是报错，索性使用yum upgrade把系统升级到最新，当然还是没有解决问题。
    
    最后通过：
    
    `yum install openssl-devel`
    
    搞定。
    
- centos查看yum操作历史：

   https://linux.cn/article-8236-1.html
    